# -*- coding: utf-8 -*-
"""
Created on Thu Nov 28 16:40:08 2019

@author: da.salazarb
"""
# %% In[*** Loading packages, data and parameters + MMjNMF algorithm ***]
# %reset_selective -f "^(?!merged)"
import os
import sys
import numpy as np
import pandas as pd
from csv import writer
import fusion_CCLE_TCGA
from sklearn.model_selection import ParameterGrid
# %%
arguments = sys.argv[1:];
if len(arguments) == 0:
    print("------------------------------------------------------------------------------------")
    print("At least include the path/to/mmjnmf, e.g., python workingSpaceOmic.py path/to/mmjnmf")
    print("")
    print("")
    # break
elif len(arguments) == 1:
    print("---------------------------------------------------------------------------------------------------")
    print("It will run M&M-jNMF with the default parameters, please check how to modify them in the README.md.")
    print("")
    print("")
    path = arguments[0]
    param_grid = {'K': [60], 
                  'r1': [3.5e-6], 'r2': [3.5e-6],
                  'L1': [10], 'L2': [10], 
                  'd1': [3.5e-3],
                    'pac_por_comod': [30], 'pac_por_comod_ccle': [25]
                  };
elif len(arguments) > 1 and len(arguments) < 7:
    print("---------------------------------------------------------------------------------------------")
    print("There are seven parameters required to execute M&M-jNMF: path/to/mmjnmf K r1 r2 L1 L2 d1")
    print("For example include them as follows: path/to/mmjnmf [60] [3.5e-6] [3.5e-6] [10] [10] [3.5e-3]")
    print("")
    print("")
elif len(arguments) == 7:
    print("--------------------------------------------------")
    print("Ok ... M&M-jNMF will be executing in a moment ... ")
    print("")
    print("")
    path = arguments[0]
    param_grid = {'K': [int(item) for item in arguments[1].strip('][').split(',')], 
                  'r1': [float(item) for item in arguments[2].strip('][').split(',')], 'r2': [float(item) for item in arguments[3].strip('][').split(',')],
                  'L1': [float(item) for item in arguments[4].strip('][').split(',')], 'L2': [float(item) for item in arguments[5].strip('][').split(',')], 
                  'd1': [float(item) for item in arguments[6].strip('][').split(',')],
                    'pac_por_comod': [30], 'pac_por_comod_ccle': [25]
                  };

#%%
## profiles!!
#path = os.path.dirname(os.getcwd()).replace("\\", "/")+"/"; #print(path)## e.g. path = "D:/"
merged = fusion_CCLE_TCGA.fusion_CCLE_TCGA(path);
merged.loadData();

## Constraints!!
merged.constraints_theta_method(path);
merged.constraints_r_method(path);
# %%
from MMJNMF import mmjnmf
tabla = pd.DataFrame(); numero = 0; useMalaCard=False;
np.random.seed(13)

grid = ParameterGrid(param_grid); #len(grid)

for params in grid: 
    print("\n                ************************************************* ")
    print("                *                                               * ")
    print('                                 '+' i = '+str(numero+1)+' of '+str(len(grid))+'                            ')
    print("                *                                               * ")
    print("                ************************************************* ")
    param = dict();
    
    param['K']=params['K'];
    param['r1']=params['r1']; param['r2']=params['r2'];
    
    param['L1']=params['L1']; param['L2']=params['L2'];
    param['d1']=params['d1'];
    
    param['saveReplicas']=False; param['loadReplicas']=False;
    
    param['tol']=1e-7; param['maxiter']=500;
    param['repeat']=3; param['sample']=""; param['saveBest_H_W']=True;
    param['pac_por_comod']=params['pac_por_comod']; param['pac_por_comod_ccle']=params['pac_por_comod_ccle'];
    
    paramResults = mmjnmf(merged, param);
    
    ## Residual sum of squares
    rss_ccle = {'rss_'+merged.project1+'_'+k: sum(sum( (v-(np.dot(paramResults['best_W']["W0_ccle"],paramResults['best_H'][k])))**2 )) for k, v in paramResults['ccle'].items()};
    rss_tcga = {'rss_'+merged.project2+'_'+k: sum(sum( (v-(np.dot(paramResults['best_W']["W0_tcga"],paramResults['best_H'][k])))**2 )) for k, v in paramResults['tcga'].items()};
    
    r2_ccle = {'r2_'+merged.project1+'_'+k: 1-((rss_ccle['rss_'+merged.project1+'_'+k] * v.shape[0] * v.shape[1])/(sum(sum( v**2 )) * ((v.shape[0]*v.shape[1]) - param['K'] * (v.shape[0] + v.shape[1]))) ) for k, v in paramResults['ccle'].items()};
    r2_tcga = {'r2_'+merged.project2+'_'+k: 1-((rss_tcga['rss_'+merged.project2+'_'+k] * v.shape[0] * v.shape[1])/(sum(sum( v**2 )) * ((v.shape[0]*v.shape[1]) - param['K'] * (v.shape[0] + v.shape[1]))) ) for k, v in paramResults['tcga'].items()};
    
    ## revisar https://stats.stackexchange.com/questions/111205/how-to-choose-an-optimal-number-of-latent-factors-in-non-negative-matrix-factori
    ## para validacion
    
    parcial = []
    for v in paramResults['rho_C'].values():
        parcial.append(v)
        
    if tabla.empty:
        for valor in ['K', 'r1', 'L1', 'L2', 'r2', 'd1', 'No. repetitions', 'tolerance', 'last_stop_control', 'ObjectiveFunction']:
            tabla.insert(tabla.shape[1],valor, None)
        for perfil in paramResults['rho_C'].keys():
            tabla.insert(tabla.shape[1],'rho_'+perfil, None)
            
        tempLista = [rss_ccle, rss_tcga ,r2_ccle, r2_tcga]
        for l in tempLista:
            temp_l = [k for k in l.keys()]; ## rss_ccle
            for valor in temp_l:
                tabla.insert(tabla.shape[1], valor, None)
        
    tabla.loc[numero] = [param['K'], param['r1'], param['L1'], param['L2'], param['r2'], param['d1'], param['pac_por_comod'], param['tol'], paramResults['best_stop_control'][0,-1], 
                         paramResults['FO']] + parcial + [v for v in rss_ccle.values()] + [v for v in rss_tcga.values()] + [v for v in r2_ccle.values()] + [v for v in r2_tcga.values()]; # + [v for v in paramResults['silhouette_avg'].values()] 
    
    if os.path.isfile(path+"/pathFeatureLabel/co-mod_tabulated_results/Tabulated_results_for_"+str(merged.listaPerfiles)+".csv") == False:
        tabla.to_csv(path+"/pathFeatureLabel/co-mod_tabulated_results/Tabulated_results_for_"+str(merged.listaPerfiles)+".csv")
    else:
        with open(path+"/pathFeatureLabel/co-mod_tabulated_results/Tabulated_results_for_"+str(merged.listaPerfiles)+".csv", 'a+', newline='') as write_obj:
            csv_writer = writer(write_obj)
            csv_writer.writerow(np.hstack([numero,tabla.iloc[numero,:]]))
     
    numero += 1;
    
print("Ok, Tabulated_results_for_"+str(merged.listaPerfiles)+".csv is saved in /pathFeatureLabel/co-mod_tabulated_results!")
print("Ok, Variable clusters are saved in individual folders in pathFeatureLabel folder, e.g., 0_drug folder!")
print("Ok, Observations clusters are saved in /pathFeatureLabel/co-mod_observations_clusters!")
